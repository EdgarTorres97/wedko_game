﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GUI_Demo::Start()
extern void GUI_Demo_Start_m6753CC1A6D2044AF9C77B4C7273F94304B68E462 (void);
// 0x00000002 System.Void GUI_Demo::OnGUI()
extern void GUI_Demo_OnGUI_mEB66A73B367C1BCF6D9A1BAEC0561603C34141B7 (void);
// 0x00000003 System.Void GUI_Demo::DoMyWindow(System.Int32)
extern void GUI_Demo_DoMyWindow_m45D8A48E8733E60FB231419B8874EC28E0F3A1E7 (void);
// 0x00000004 System.Void GUI_Demo::.ctor()
extern void GUI_Demo__ctor_m19ED6E96FF715751F68BEE7444656308611A14A3 (void);
// 0x00000005 System.Void AlEntrarHaz::Start()
extern void AlEntrarHaz_Start_m9F635CACA72CAE5A6D60A0C68A5AE564FFB668DA (void);
// 0x00000006 System.Void AlEntrarHaz::OnTriggerEnter(UnityEngine.Collider)
extern void AlEntrarHaz_OnTriggerEnter_m04ABB7313E2B0E437291A99F8BD4E40F12B1A2CD (void);
// 0x00000007 System.Void AlEntrarHaz::.ctor()
extern void AlEntrarHaz__ctor_m2FEDCC195152E9D9772C433E0838AA30F19ECB89 (void);
// 0x00000008 System.Void CambiarESCENA::Start()
extern void CambiarESCENA_Start_m8311E18BAAC1B7E76D0AD56781F29AF640E77F72 (void);
// 0x00000009 System.Void CambiarESCENA::OnTriggerEnter(UnityEngine.Collider)
extern void CambiarESCENA_OnTriggerEnter_m745AB72DD6B4A659F5B3C8A4A1EF3E268EDBD526 (void);
// 0x0000000A System.Void CambiarESCENA::.ctor()
extern void CambiarESCENA__ctor_m4EDB862EAFBA761363D8F1C27BB2D5DE3E00F7B0 (void);
// 0x0000000B System.Void Cerrar::Start()
extern void Cerrar_Start_m7E0824BD68235AAB21E04B69FBEB8A5CBB1C1CAC (void);
// 0x0000000C System.Void Cerrar::Update()
extern void Cerrar_Update_m80E3953062D6A5DC3F2E8225D5D78DFB8ECE32C5 (void);
// 0x0000000D System.Void Cerrar::Exit()
extern void Cerrar_Exit_mC68694FFD94A36CCE68F23F46D60EF9D9197C149 (void);
// 0x0000000E System.Void Cerrar::.ctor()
extern void Cerrar__ctor_m988A2895D9B5FE7495DF9CE8D1FBC70237CA08C6 (void);
// 0x0000000F System.Void ContadorMonedas::Awake()
extern void ContadorMonedas_Awake_m643330691B1E56ABF20654BFCE4FB373877FC9C9 (void);
// 0x00000010 System.Void ContadorMonedas::Start()
extern void ContadorMonedas_Start_m3B007FF2048B5F51A07695C6A702E05153C7BF4B (void);
// 0x00000011 System.Void ContadorMonedas::Update()
extern void ContadorMonedas_Update_m49E77A6B75F51A5709484960AECDC1BCE7EFC030 (void);
// 0x00000012 System.Void ContadorMonedas::Collect()
extern void ContadorMonedas_Collect_m329A1D8D0DD460C1F38DDF6A3DEEB3F1AE1F966D (void);
// 0x00000013 System.Void ContadorMonedas::RefreshUI()
extern void ContadorMonedas_RefreshUI_mB2BB71F9238844BA087FD6BEDADC93DCD36764C2 (void);
// 0x00000014 System.Void ContadorMonedas::OnDestroy()
extern void ContadorMonedas_OnDestroy_mBE89F7D3C03292E9C6627332CE0A84A920C81F22 (void);
// 0x00000015 System.Void ContadorMonedas::SaveData()
extern void ContadorMonedas_SaveData_m9E1E2F05D5CEE7D9C1651CEB0DB5EE79B26EAC14 (void);
// 0x00000016 System.Void ContadorMonedas::LoadData()
extern void ContadorMonedas_LoadData_m9CDC46BDC2EFEE5AF1DFDC8057CB23BB8FC241C6 (void);
// 0x00000017 System.Void ContadorMonedas::.ctor()
extern void ContadorMonedas__ctor_m0BFBD8B42ED97BAC18D749A25E2363C69DDEFC3A (void);
// 0x00000018 System.Void ControladorTablero::Update()
extern void ControladorTablero_Update_mBC7782A56F5F6C540AB39A53B462CCB84684B427 (void);
// 0x00000019 System.Void ControladorTablero::controlarTablero()
extern void ControladorTablero_controlarTablero_m422226F108FDAD0C6352942827D3860780EB40F9 (void);
// 0x0000001A System.Void ControladorTablero::.ctor()
extern void ControladorTablero__ctor_mF690992EDC8D9633B9BCF41232D54EB16C510116 (void);
// 0x0000001B System.Void Inicio::Start()
extern void Inicio_Start_m8265D66FFDE8A22BD4A48C57F9A7378F32AD9E8E (void);
// 0x0000001C System.Void Inicio::Update()
extern void Inicio_Update_mA0EF0CF864EDB3C530660FCDB73A6457F1CCCB0A (void);
// 0x0000001D System.Void Inicio::OnRuntimeMethodLoad()
extern void Inicio_OnRuntimeMethodLoad_mFDC1889157B73A6CD660AD041BD06935FCAACDDF (void);
// 0x0000001E System.Void Inicio::.ctor()
extern void Inicio__ctor_m824B076D838FD30434A02E5CF8B5BA16C12D71C8 (void);
// 0x0000001F System.Void Pause::Start()
extern void Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC (void);
// 0x00000020 System.Void Pause::Update()
extern void Pause_Update_m2BEBA55A60F1B469B29DD631505236F1B5F21FEE (void);
// 0x00000021 System.Void Pause::Resume()
extern void Pause_Resume_m3E7596818DA043F9184F6507BB51E298B9F83487 (void);
// 0x00000022 System.Void Pause::Exit()
extern void Pause_Exit_mE58449D5CFA4046DB0F3EC6B76DE6334300BA649 (void);
// 0x00000023 System.Void Pause::.ctor()
extern void Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928 (void);
// 0x00000024 System.Void Rotar::Update()
extern void Rotar_Update_m6A1E0AB6D4DFF5637A4804472A6E037647FC9CB7 (void);
// 0x00000025 System.Void Rotar::.ctor()
extern void Rotar__ctor_m453CD30BF342021E4B8785AC51BF53C609B07A27 (void);
// 0x00000026 System.Void show_variables::Start()
extern void show_variables_Start_m8401BAC7431995A46C7B474CE8FDA76C3646F8DF (void);
// 0x00000027 System.Void show_variables::Update()
extern void show_variables_Update_m3D1F35B69D46715184CA1937DC80EC625284452C (void);
// 0x00000028 System.Void show_variables::.ctor()
extern void show_variables__ctor_mED6B9CD4D593C2BED342F33CFBE815E7A5C2024C (void);
static Il2CppMethodPointer s_methodPointers[40] = 
{
	GUI_Demo_Start_m6753CC1A6D2044AF9C77B4C7273F94304B68E462,
	GUI_Demo_OnGUI_mEB66A73B367C1BCF6D9A1BAEC0561603C34141B7,
	GUI_Demo_DoMyWindow_m45D8A48E8733E60FB231419B8874EC28E0F3A1E7,
	GUI_Demo__ctor_m19ED6E96FF715751F68BEE7444656308611A14A3,
	AlEntrarHaz_Start_m9F635CACA72CAE5A6D60A0C68A5AE564FFB668DA,
	AlEntrarHaz_OnTriggerEnter_m04ABB7313E2B0E437291A99F8BD4E40F12B1A2CD,
	AlEntrarHaz__ctor_m2FEDCC195152E9D9772C433E0838AA30F19ECB89,
	CambiarESCENA_Start_m8311E18BAAC1B7E76D0AD56781F29AF640E77F72,
	CambiarESCENA_OnTriggerEnter_m745AB72DD6B4A659F5B3C8A4A1EF3E268EDBD526,
	CambiarESCENA__ctor_m4EDB862EAFBA761363D8F1C27BB2D5DE3E00F7B0,
	Cerrar_Start_m7E0824BD68235AAB21E04B69FBEB8A5CBB1C1CAC,
	Cerrar_Update_m80E3953062D6A5DC3F2E8225D5D78DFB8ECE32C5,
	Cerrar_Exit_mC68694FFD94A36CCE68F23F46D60EF9D9197C149,
	Cerrar__ctor_m988A2895D9B5FE7495DF9CE8D1FBC70237CA08C6,
	ContadorMonedas_Awake_m643330691B1E56ABF20654BFCE4FB373877FC9C9,
	ContadorMonedas_Start_m3B007FF2048B5F51A07695C6A702E05153C7BF4B,
	ContadorMonedas_Update_m49E77A6B75F51A5709484960AECDC1BCE7EFC030,
	ContadorMonedas_Collect_m329A1D8D0DD460C1F38DDF6A3DEEB3F1AE1F966D,
	ContadorMonedas_RefreshUI_mB2BB71F9238844BA087FD6BEDADC93DCD36764C2,
	ContadorMonedas_OnDestroy_mBE89F7D3C03292E9C6627332CE0A84A920C81F22,
	ContadorMonedas_SaveData_m9E1E2F05D5CEE7D9C1651CEB0DB5EE79B26EAC14,
	ContadorMonedas_LoadData_m9CDC46BDC2EFEE5AF1DFDC8057CB23BB8FC241C6,
	ContadorMonedas__ctor_m0BFBD8B42ED97BAC18D749A25E2363C69DDEFC3A,
	ControladorTablero_Update_mBC7782A56F5F6C540AB39A53B462CCB84684B427,
	ControladorTablero_controlarTablero_m422226F108FDAD0C6352942827D3860780EB40F9,
	ControladorTablero__ctor_mF690992EDC8D9633B9BCF41232D54EB16C510116,
	Inicio_Start_m8265D66FFDE8A22BD4A48C57F9A7378F32AD9E8E,
	Inicio_Update_mA0EF0CF864EDB3C530660FCDB73A6457F1CCCB0A,
	Inicio_OnRuntimeMethodLoad_mFDC1889157B73A6CD660AD041BD06935FCAACDDF,
	Inicio__ctor_m824B076D838FD30434A02E5CF8B5BA16C12D71C8,
	Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC,
	Pause_Update_m2BEBA55A60F1B469B29DD631505236F1B5F21FEE,
	Pause_Resume_m3E7596818DA043F9184F6507BB51E298B9F83487,
	Pause_Exit_mE58449D5CFA4046DB0F3EC6B76DE6334300BA649,
	Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928,
	Rotar_Update_m6A1E0AB6D4DFF5637A4804472A6E037647FC9CB7,
	Rotar__ctor_m453CD30BF342021E4B8785AC51BF53C609B07A27,
	show_variables_Start_m8401BAC7431995A46C7B474CE8FDA76C3646F8DF,
	show_variables_Update_m3D1F35B69D46715184CA1937DC80EC625284452C,
	show_variables__ctor_mED6B9CD4D593C2BED342F33CFBE815E7A5C2024C,
};
static const int32_t s_InvokerIndices[40] = 
{
	1535,
	1535,
	1277,
	1535,
	1535,
	1287,
	1535,
	1535,
	1287,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	2421,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
	1535,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	40,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
