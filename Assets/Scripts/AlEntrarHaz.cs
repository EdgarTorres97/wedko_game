﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlEntrarHaz : MonoBehaviour {
public int valor = 1;

    public AudioClip clip;
    float volumen = 2.0f;
    Transform posicion;

    private void Start()
    {
        posicion = transform;
    }

    void OnTriggerEnter(Collider other)
    {
        AudioSource.PlayClipAtPoint(clip, posicion.position, volumen);
        ContadorMonedas.SharedInstance.Monedas ++;
        ContadorMonedas.SharedInstance.Collect();
        Destroy(this.gameObject);
    }
}
