using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject pauseWindow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            pauseWindow.SetActive(true);
        }
    }
    public void Resume()
{
        pauseWindow.SetActive(false);
    }
     public void Exit()
    {
        Application.Quit();
    }
}
