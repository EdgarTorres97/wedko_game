using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarESCENA : MonoBehaviour
{
    public int escena;
    public AudioClip clip;
    float volumen = 1.0f;
    Transform posicion;

    private void Start()
    {
        posicion = transform;
    }


    void OnTriggerEnter(Collider other)
    {
        AudioSource.PlayClipAtPoint(clip, posicion.position, volumen);
        Destroy(this.gameObject);
        SceneManager.LoadScene(escena);


    }
}
