using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContadorMonedas : MonoBehaviour

{
    public int money;
    public int items;
   

    private string moneyPrefsName ="Money";
    private string itemsPrefsName = "Items";
    
    //private UserInterface userInterface;

    private int collectableValue = 1;
    private int itemCost = 1;

    public static ContadorMonedas SharedInstance;
    [Tooltip("Puntos:")]
    public int Monedas;
    public void Awake()
    
    {
        LoadData();
        if (SharedInstance == null){
            SharedInstance = this;
        }
        else{
            Debug.Log(message: "no se realizo", gameObject);
            
            Collect();
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
         //userInterface = GameObject.FindGameObjectWithTag("GameController").GetComponent;
        //RefreshUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Collect()
    {
        money += collectableValue;
        //RefreshUi();
    }

    private void RefreshUI()
    {
        //userInterface.RefreshMoney(money);
        //userInterface.RefreshItems(items);
    }
    private void OnDestroy()
    {
        SaveData();
    }
    private void SaveData()
    {
        PlayerPrefs.SetInt(moneyPrefsName,money);
        PlayerPrefs.SetInt(itemsPrefsName,items);
    }

    private void LoadData()
    {
        money = PlayerPrefs.GetInt(moneyPrefsName,0);
        items = PlayerPrefs.GetInt(itemsPrefsName,0);

    }
}
